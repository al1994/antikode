1. clone project pada branch master
2. copy .env.example dan paste dengan nama .env
3. buat db dan sesuaikan dengan nama db pada .env
4. composer install
5. php artisan php artisan migrate (untuk membuat table)
6. php artisan db:seed (untuk mengisi data db)
7. php artisan serve


Task

    1. Brand name
    endpoint = http://localhost:8000/api/brand/name

    2. Outlet name, address, longitude, latitude
    endpoint = http://localhost:8000/api/outlet

    3. Total product
    endpoint = http://localhost:8000/api/product/count

    4. Distance Outlet position from Monas Jakarta in Kilometers
    endpoint = http://localhost:8000/api/outlet/latfrom/-6.1753716/longfrom/104.7779529

    5. Each brand will have multiple outlets and multiple products
    endpoint = http://localhost:8000/api/brand

    6. Sort by distance closest to Monas
    endpoint = http://localhost:8000/api/outlet/latfrom/-6.175392/longfrom/106.827153/sort

    7. API in Graphql is a plus
    -



